<%@ Control language="cs" AutoEventWireup="false" Explicit="True" Inherits="DotNetNuke.UI.Skins.Skin" %>

<!--#include file="Common/Common.ascx"-->

<div class="container">
<!--#include file="Common/ProfileLink.ascx"-->
<!--#include file="Common/Menu.ascx"-->
    <main class="maincontent">

        <section class="photos-content-section">
            <div id="ContentTitlePane" runat="server"></div>
              <div id="ContentPane" runat="server" class="hub-gallery-wrapper four-items photo-gallery"></div>
        </section>

    </main>
</div>
