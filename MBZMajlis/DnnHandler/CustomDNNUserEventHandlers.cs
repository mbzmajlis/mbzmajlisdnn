﻿using DotNetNuke.Entities.Users;
using MBZMajlis.Service;
using System.ComponentModel.Composition;

namespace MBZMajlis
{
    [Export(typeof(IUserEventHandlers))]
    public class CustomDNNUserEventHandlers : IUserEventHandlers
    {
        public void UserAuthenticated(object sender, UserEventArgs args)
        {
            
        }

        public void UserCreated(object sender, UserEventArgs args)
        {
        }

        public void UserDeleted(object sender, UserEventArgs args)
        {
        }

        public void UserRemoved(object sender, UserEventArgs args)
        {
        }

        public void UserApproved(object sender, UserEventArgs args)
        {
            EmailService.SendEmailToApprovedUser(args.User);
        }

        public void UserUpdated(object sender, UpdateUserEventArgs args)
        {
          
        }
    }

}