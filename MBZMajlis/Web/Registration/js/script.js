

function submitRegistartionData() {
     

        let firstName = $('input[name="firstName"]').val();
        let middleName = $('input[name="middleName"]').val();
        let familyName = $('input[name="familyName"]').val();
        let gender = $('select[name="gender"]').val();

        let email = $('input[name="mail"]').val();
        let emirate = $('select[name="emirate"]').val();

        let endrolledUniversity = $('select[name="endrolled-university"]').val(); 
        let country = $('select[name="country"]').val(); 

        let birthdayDate = $('input[name="birthday"]').datepicker('getDate');

        let birthday = null;
        if (birthdayDate != null) {
            let d = String(birthdayDate.getDate()).padStart(2, '0');
            let m = String(birthdayDate.getMonth() + 1).padStart(2, '0'); //January is 0!
            let y = birthdayDate.getFullYear();
          birthday = d + '/' + m + '/' + y;
        }

        let institution = '';
        let level = '';
        let studyField = '';

        if (!email || email == '') {
            $("#register-button").prop("disabled", false);
            return;
        }

        if (country === "UAE") {
            institution = $('select[name="institution"]').val();
            level = $('select[name="level"]').val();
            studyField = $('select[name="study-field"]').val();
        }
        else {
            institution = $('input[name="institution"]').val();
            level = $('input[name="level"]').val();
            studyField = $('input[name="study"]').val();
        }

        let dto = {
            FirstName : firstName,
            MiddleName : middleName,
            FamilyName : familyName,
            Gender: gender,
            Email: email,
            Emirate: emirate,
            EndrolledUniversity: endrolledUniversity,
            Country: country,
            Institution: institution,
            Level: level,
            StudyField: studyField,
            Birthday: birthday
        };

        $('body').addClass('preload');
       
        $.ajax({
            type: "POST",
            url: "/API/MBZMajlis/Registration/Register",
            data: JSON.stringify(dto),
            contentType: 'application/json',
            success: function () {
                                
                // Disappear registration form and appear thank you text instead
                $('.registration-form-content').fadeOut(150, function () {
                    $('.thankyouPart').fadeIn(150);
                });

                $('body').removeClass('preload');
                
            }, 
            error: function (message) {
              
               // appendResponse(errorMessages.registration.formType, errorMessages.contact.error);
                $('body').removeClass('preload');
            }
           
        });
    }
