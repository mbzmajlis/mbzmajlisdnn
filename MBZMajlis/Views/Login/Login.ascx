﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Login.ascx.cs" Inherits="MBZMajlis.Views.Login.Login" %>

<% Func<string, string> GetString = (s) => DotNetNuke.Services.Localization.Localization.GetString(s, "~/App_GlobalResources/MBZMajlis.resx"); %>
<%--<div>
    <p class="email-group">
        <input type="email" name="email" placeholder="Email" required=""  id="email">
    </p>
    <p>
        <input type="password" name="password" placeholder="Password" required=""  id="password">
    </p>


   <input type="submit" value="Log in" id="button-login">

</div>--%>
<%--<div class="form-response-output"></div>--%>

<script type="text/javascript">
    var submit = 0;
    function CheckDouble() {
        if (++submit > 1) {
           // alert('This sometimes takes a few seconds - please be patient.');
            return false;
        }

        setTimeout(function () { submit = 0;}, 3000);
    }

    var submit2 = 0;
    function CheckDouble2() {
        if (++submit2 > 1) {
            // alert('This sometimes takes a few seconds - please be patient.');
            return false;
        }
        setTimeout(function () { submit2 = 0; }, 3000);
    }
</script>

<asp:UpdatePanel ID="updatePnl" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                    <form name="LogInForm">
                    <div>
                        <div runat="server" id="Step1">
                            <p class="email-group">
                                <input type="email" name="email" required="" runat="server" id="email"/>
                            </p>
                            <p class="password-group">
                                <input type="password" name="password" required="" runat="server" id="password"/>
                            </p>
                            <asp:Button ID="btLogIn1" OnClick="LogIn_Click" runat="server" OnClientClick="return CheckDouble();"/>
                        </div>
                        <div runat="server" id="Step2" visible="false">
                            A verification code has been sent to your email
                            <p class="password-group">
                                <input type="text" name="code" required="" runat="server" id="code"/>
                            </p>
                             <asp:Button ID="btLogIn2" OnClick="LogIn_Click2" runat="server" OnClientClick="return CheckDouble2();"/>
                        </div>
                        
                       
                    </div>

                    <div class="form-response-output">
                        <asp:Label runat="server" ID="lblError" />
                    </div>
  
                     </form>
                </ContentTemplate>
   </asp:UpdatePanel>
                          

<%-- <script src='<%= ResolveUrl("../../Web/Login/js/script.js") %>'></script>--%>
