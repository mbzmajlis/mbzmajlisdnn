﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace MBZMajlis.dbContext
{
    public class DNNContext : System.Data.Entity.DbContext
    {
        public DNNContext()
          : base("SiteSqlServer")
        { }

        public DbSet<entRegistrationData> RegistrationData { get; set; }
        public DbSet<entContactUsData> ContactUsData { get; set; }
        public DbSet<entDigest> Digest { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {

            modelBuilder.Entity<entRegistrationData>()
              .ToTable("tblRegistrationData");
            modelBuilder.Entity<entRegistrationData>()
                .HasKey(x => x.PKey);


            modelBuilder.Entity<entContactUsData>()
              .ToTable("tblContactUsData");
            modelBuilder.Entity<entContactUsData>()
                .HasKey(x => x.PKey);

            modelBuilder.Entity<entDigest>()
              .ToTable("tblDigest");
            modelBuilder.Entity<entDigest>()
                .HasKey(x => x.PKey);


            base.OnModelCreating(modelBuilder);
        }
    }
}